.. _resources:

Online Resources and References
-------------------------------

This is a preliminary list of campus and web resources for the course. Please send me links or references to related material you encounter that may interest others in the class.


UW Madison Campus Resources
~~~~~~~~~~~~~~~~~~~~~~~~~~~

- `Social Science Computing Cooperative`_
- `Madison Data Science Initiative (MadDSI)`_
- `Lynda.com`_
- `DoIT Software Training for Students (STS)`_
- `Advanced Computing Initiative Learning Resources`_


Python Online Resources for working with Data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- `Computational and Inferential Thinking`_ Adhikari, Ana and John DeNero with Contributions by David Wagner
- `Turi Maching Learning Platform User Guide`_ Fox, Emily and Carlos Guestrin
- `Big Data and Social Science`_ Foster, Ian, Rayid Ghani, Ron Jarmin, Frauke Kreuter, and Julia Lane (or purchase at `CRC Press`_)
- `Data Bootcamp`_ NYU Stern School of Business
- `SciKit Learn Tutorial`_ and `Python Data Science Handbook`_, Jake VanderPlas
- `Introducing Python`_ and associated `GitHub repository`_, Bill Lubanovic
- `Awesome Python Tools`_, Gregory Petukhov

.. _Social Science Computing Cooperative: https://www.ssc.wisc.edu/sscc
.. _Madison Data Science Initiative (MadDSI): https://sites.google.com/site/dsiuwmadison
.. _Lynda.com: https://it.wisc.edu/services/online-training-lynda-com
.. _DoIT Software Training for Students (STS): https://at.doit.wisc.edu/training/software-training-for-students
.. _Advanced Computing Initiative Learning Resources: https://aci.wisc.edu/resources/#learning-overview
.. _Linux Director Structure Basics: https://www.lynda.com/Linux-tutorials/Linux-Directory-Structure-Basic-Tools/517444-2.html
.. _Linux Bash Shell Scripting: https://www.lynda.com/Linux-tutorials/Linux-Bash-Shell-Scripts/504429-2.html
.. _Linux Command Line Basics: https://www.lynda.com/Linux-tutorials/Learn-Linux-Command-Line-Basics/435539-2.html
.. _Linux Remote Computing: https://www.lynda.com/Linux-tutorials/Linux-Multitasking-Command-Line/519671-2.html
.. _SQL Data Reporting and Analysis: https://www.lynda.com/SQL-tutorials/SQL-Data-Reporting-Analysis/529631-2.html
.. _Computational and Inferential Thinking: https://www.inferentialthinking.com
.. _Turi Maching Learning Platform User Guide: https://turi.com/learn/userguide
.. _Big Data and Social Science: https://github.com/CSSIP-AIR/Big-Data-Workbooks
.. _CRC Press: https://www.crcpress.com/Big-Data-and-Social-Science-A-Practical-Guide-to-Methods-and-Tools/Foster-Ghani-Jarmin-Kreuter-Lane/p/book/9781498751407
.. _Data Bootcamp: https://github.com/DaveBackus/Data_Bootcamp
.. _SciKit Learn Tutorial: https://github.com/jakevdp/sklearn_pycon2015
.. _Python Data Science Handbook: https://github.com/jakevdp/PythonDataScienceHandbook
.. _Introducing Python: https://www.safaribooksonline.com/library/view/introducing-python/9781449361167/
.. _GitHub repository: https://github.com/madscheme/introducing-python
.. _Awesome Python Tools: https://github.com/lorien/awesome-web-scraping/blob/master/python.md
