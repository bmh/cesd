Course Topics
-------------

+------------------------------------------+-----------------------------------------------------------------+
| Data science: a new discipline?          | - What disciplines are feeding this new science?                |
|                                          | - How can this new science help social scientists?              |
+------------------------------------------+-----------------------------------------------------------------+
| Computational methods for data science   | - Classification and clustering                                 |
|                                          | - Topic modeling                                                |
|                                          | - Natural language processing                                   |
|                                          | - Linking and matching                                          |
|                                          | - Network analysis                                              |
+------------------------------------------+-----------------------------------------------------------------+
| The graphical and visual display of data | - Static plots                                                  |
|                                          | - Programmatic interface and reproducibility                    |
|                                          | - Interactive web-based plots                                   |
+------------------------------------------+-----------------------------------------------------------------+
| Sources of data for the social sciences  | - Survey, secondary, experimental                               |
|                                          | - Data science as a source for new data                         |
|                                          | - Public and private administrative records                     |
|                                          | - On-line experiments                                           |
|                                          | - Streaming data                                                |
+------------------------------------------+-----------------------------------------------------------------+
|                                          | - Descriptive versus structural analysis                        |
| Integrating Data and Social Sciences     | - Predictive verus structural analysis                          |
|                                          | - Counterfactual analysis                                       |
+------------------------------------------+-----------------------------------------------------------------+
| Reproducibility and transparency         | - Programming and code documentation                            |
|                                          | - Presenting results in notebooks and other interactive formats |
|                                          | - On-line commenting and continuous updating                    |
+------------------------------------------+-----------------------------------------------------------------+


