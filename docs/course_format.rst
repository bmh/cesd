Course Format
-------------

I will assign readings, videos, and exercises each week, and I will administer a short in-class quiz at the beginning of each weekly class (assuming we meet once per week; otherwise, the quiz will be at the beginning of the last class of the week). We will devote the remainder of class time to working on projects.

I will encourage each student to bring their own data and project ideas to class, and to form a group with other students who have complimentary interests and skills. Students can work independently, if desired. I will work with students who do not have data of their own to identify project ideas and potential data sources. Reading and viewing material each week will provide opportunities to explore new strategies to examine and work with project data.

The final course project will be comprised of a fully documented data analysis project that provides data documentation (including metadata relevant to data provenance, source description, etc.), data cleaning and preparation, analysis, written report *that is fully reproducible* (here are two excellent examples for researchers in Bioinformatics_  and `Digital Humanities`_; see here_ for blog post summary of the content in the second of these projects), and presentation.

.. _Bioinformatics: https://github.com/rhiever/Data-Analysis-and-Machine-Learning-Projects/blob/master/example-data-science-notebook/Example%20Machine%20Learning%20Notebook.ipynb

.. _Digital Humanities: https://github.com/tedunderwood/paceofchange

.. _here: https://tedunderwood.com/2015/05/18/how-quickly-do-literary-standards-change/

