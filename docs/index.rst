.. Computational Methods for Economic and Social Data documentation master
   file, created by sphinx-quickstart on Tue Jan 10 12:57:29 2017. You can
   adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Computational Methods for Economic and Social Data
==================================================

- **Instructor**: Brent Hueth, Assoc. Prof, Ag. & Applied Economics

- **Email**: hueth@wisc.edu

- **Phone**: (608) 890-0924

- **Office**: 518 Taylor Hall

- `Announcements and Discussion`_

- `Datacamp assignments`_

- `Gitlab course repository`_


.. include:: description.rst
.. include:: objectives.rst
.. include:: topics.rst
.. include:: prerequisites.rst

.. toctree::
   :maxdepth: 1
   :hidden:

   objectives.rst
   topics.rst
   prerequisites.rst
   schedule.rst
   motivation.rst
   course_format.rst
   resources.rst
   references.rst

.. _Announcements and Discussion: https://groups.google.com/forum/#!forum/cesd-spring-2017

.. _Datacamp assignments: https://www.datacamp.com/groups/computational-methods-for-economic-and-social-data/assignments

.. _Gitlab course repository: https://gitlab.com/brenthueth/computing-economic-social-data

.. ref::`genindex`
.. ref::`search`

.. announcements, discussion, assignments, solutions, lecture notes
