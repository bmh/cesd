Course Schedule
---------------

**January 30**: Introductions and coordination. Assignments for next week

- `Intro to Python for Data Science`_
- `Importing Data in Python (Part 1)`_

**February 6**: Review assignments; Google Compute VM (we want to work with our own data); in class work on `Intermediate Python for Data Science`_ (Ch. 2-4). Assignments for next week

- `Intro to Unix`_ (see `here`_ if you don't UW login); you can use your Google VM if you like
- `Importing Data in Python (Part 2)`_

**February 13** Review assignments. Setup `Datalab`_ on the GC platform and local machines. `Intermediate Python for Data Science`_ chp. (1 and 5).  And to keep you busy during the next couple of weeks:

- `Whirlwind Tour of Python Notebooks`_
- `Introduction to Databases in Python`_
- `Statistical Thinking in Python (Part 1)`_
- `Statistical Thinking in Python (Part 2)`_

**February 20**: No meeting

**February 27**: No meeting

**March 6**: `Scikit-learn tutorial`_

**March 13**: Develop projects

**March 20 (Spring Break)**

**March 27**

**April 3**

**April 10**

**April 24**: No meeting

**May 1**

**May 8**

.. _Whirlwind Tour of Python Notebooks: https://github.com/jakevdp/WhirlwindTourOfPython
.. _Datalab: https://github.com/googledatalab/datalab
.. _Intro to Python for Data Science: https://www.datacamp.com/courses/intro-to-python-for-data-science
.. _Importing Data in Python (Part 1): https://www.datacamp.com/courses/importing-data-in-python-part-1
.. _Intermediate Python for Data Science: https://www.datacamp.com/courses/intermediate-python-for-data-science
.. _Importing Data in Python (Part 2): https://www.datacamp.com/courses/importing-data-in-python-part-2
.. _Statistical Thinking in Python (Part 1): https://www.datacamp.com/courses/statistical-thinking-in-python-part-1
.. _Statistical Thinking in Python (Part 2): https://www.datacamp.com/courses/statistical-thinking-in-python-part-2
.. _Introduction to Databases in Python: https://www.datacamp.com/courses/introduction-to-relational-databases-in-python
.. _Intro to Unix: https://www.lynda.com/Linux-tutorials/Learn-Linux-Command-Line-Basics/435539-2.html?org=wisc.edu
.. _here: https://software.rc.fas.harvard.edu/training/workshop_intro_unix/latest
.. _Scikit-learn tutorial: https://github.com/jakevdp/sklearn_tutorial
