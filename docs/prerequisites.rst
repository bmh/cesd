Prerequisites
-------------

The course is primarily intended for graduate students in the social sciences, but I will consider admitting upper-level undergraduates who demonstrate a strong interest in the course content.

I aim to make the course accessible to students with wide-ranging interests and computational knowledge. It is my hope that a sufficiently diverse group of students enroll so that collectively I can identify and form groups that operate as data-science teams collaborating toward completion of a course project that draws on the diverse talents of all group members.

However, during the first several weeks of the course I will expect students to develop basic familiarity with:

* File management and system administration in a Unix (e.g., Linux or macOS) environment
* Basic syntax for the Python programming language
* Structured Query Language (SQL)
* Git for version control

There are many fantastic places on this campus (see :ref:`resources`), and on the web (often for free, or small monthly charge) where you can acquire basic working knowledge of these tools. If you know absolutely nothing about any of these topics, you should plan to allocate time for self-study during the early part of the course. However, please don't worry if you're just getting started; there is always more to learn, and I will encourage lots of classroom interaction to share knowledge.

