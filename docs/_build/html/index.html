

<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->
<head>
  <meta charset="utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <title>Computational Methods for Economic and Social Data &mdash; Computational Methods for Economic and Social Data</title>
  

  
  

  

  
  
    

  

  
  
    <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
  

  

  
        <link rel="index" title="Index"
              href="genindex.html"/>
        <link rel="search" title="Search" href="search.html"/>
    <link rel="top" title="Computational Methods for Economic and Social Data" href="#"/>
        <link rel="next" title="Learning Objectives" href="objectives.html"/> 

  
  <script src="_static/js/modernizr.min.js"></script>

</head>

<body class="wy-body-for-nav" role="document">

  <div class="wy-grid-for-nav">

    
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search">
          

          
            <a href="#" class="icon icon-home"> Computational Methods for Economic and Social Data
          

          
          </a>

          
            
            
          

          
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>

          
        </div>

        <div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="main navigation">
          
            
            
                <ul>
<li class="toctree-l1"><a class="reference internal" href="objectives.html">Learning Objectives</a></li>
<li class="toctree-l1"><a class="reference internal" href="topics.html">Course Topics</a></li>
<li class="toctree-l1"><a class="reference internal" href="prerequisites.html">Prerequisites</a></li>
<li class="toctree-l1"><a class="reference internal" href="schedule.html">Course Schedule</a></li>
<li class="toctree-l1"><a class="reference internal" href="motivation.html">Why Take This Class?</a></li>
<li class="toctree-l1"><a class="reference internal" href="course_format.html">Course Format</a></li>
<li class="toctree-l1"><a class="reference internal" href="resources.html">Online Resources and References</a></li>
<li class="toctree-l1"><a class="reference internal" href="references.html">References</a></li>
</ul>

            
          
        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap">

      
      <nav class="wy-nav-top" role="navigation" aria-label="top navigation">
        <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
        <a href="#">Computational Methods for Economic and Social Data</a>
      </nav>


      
      <div class="wy-nav-content">
        <div class="rst-content">
          

 



<div role="navigation" aria-label="breadcrumbs navigation">
  <ul class="wy-breadcrumbs">
    <li><a href="#">Docs</a> &raquo;</li>
      
    <li>Computational Methods for Economic and Social Data</li>
      <li class="wy-breadcrumbs-aside">
        
          
            <a href="_sources/index.rst.txt" rel="nofollow"> View page source</a>
          
        
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
            
  <div class="section" id="computational-methods-for-economic-and-social-data">
<h1>Computational Methods for Economic and Social Data<a class="headerlink" href="#computational-methods-for-economic-and-social-data" title="Permalink to this headline">¶</a></h1>
<ul class="simple">
<li><strong>Instructor</strong>: Brent Hueth, Assoc. Prof, Ag. &amp; Applied Economics</li>
<li><strong>Email</strong>: <a class="reference external" href="mailto:hueth&#37;&#52;&#48;wisc&#46;edu">hueth<span>&#64;</span>wisc<span>&#46;</span>edu</a></li>
<li><strong>Phone</strong>: (608) 890-0924</li>
<li><strong>Office</strong>: 518 Taylor Hall</li>
<li><a class="reference external" href="https://groups.google.com/forum/#!forum/cesd-spring-2017">Announcements and Discussion</a></li>
<li><a class="reference external" href="https://www.datacamp.com/groups/computational-methods-for-economic-and-social-data/assignments">Datacamp assignments</a></li>
<li><a class="reference external" href="https://gitlab.com/brenthueth/computing-economic-social-data">Gitlab course repository</a></li>
</ul>
<p>This course bridges computational and social sciences with a goal to increase the programming and computational skills of students in the social sciences broadly.</p>
<p>Motivation for the course comes from new data challenges associated with modern communication, sensoring, and internet-based technologies. These technologies present researchers, businesses, and public agencies with new opportunities for data-based inquiry and decision making. It is an irony, but not a coincidence, however, that increased production and availability of data (arguably) is causing increased levels skepticism, even disbelief, in the general population regarding the veracity of data-based discovery and knowledge. These new opportunities therefore present many new challenges for traditional modes of data analysis and reporting.</p>
<p>During the onset of the Big Data era, capturing and storing terabyte and petabyte scale data represented a first-order concern. This was a job for engineers and computer scientists. Organizing, classifying, and drawing inferences from these data came later, and in this realm there is ample space, and need, for skill sharing, communication, and cross fertilization of ideas among a wide range of science domains. I hope this course can contribute toward filling this space.</p>
<div class="section" id="learning-objectives">
<h2>Learning Objectives<a class="headerlink" href="#learning-objectives" title="Permalink to this headline">¶</a></h2>
<p>Interdisciplinarity forces compromise with respect to depth of coverage. My aim is to make students from business and social science fields familiar enough with computational methods relevant to working with unstructured and &#8220;Big&#8221; data that they know what is possible, where to look, and what questions to ask when seeking solutions to seemingly intractable data challenges; for students from the computational sciences, I aim to provide increased awareness and understanding of how theory of behavior and markets can be used when building data models and interpreting estimation results.</p>
<p>Specific learning objectives include:</p>
<ul class="simple">
<li><strong>Programmatic data retrieval</strong>: Use Python tools to extract data from HTML, XML, and JSON file formats, and to connect with and acquire data from web-based data APIs.</li>
<li><strong>Data processing tools</strong>: Provision and use cloud computing resources; basic familiarity with Unix command line tools for file management and data handling; Python programming language for data analysis; essentials of database query.</li>
<li><strong>Version control</strong>: Familiarity and elementary use of Git file versioning, repository management, and collaboration tools.</li>
<li><strong>Text processing</strong>: Use command line scripting, text-based document preparation tools, and the Python programming language to automate data analysis and manuscript preparation.</li>
<li><strong>Computational methods for data analysis</strong>: Demonstrate ability to apply one or more Data Science tools (e.g., classification, clustering, natural language processing) to data, and to interpret results appropriately.</li>
<li><strong>Social science theory, model interpretation, and inference</strong>: Use a social science theory or model to guide and facilitate data analysis and interpretation.</li>
</ul>
</div>
<div class="section" id="course-topics">
<h2>Course Topics<a class="headerlink" href="#course-topics" title="Permalink to this headline">¶</a></h2>
<table border="1" class="docutils">
<colgroup>
<col width="39%" />
<col width="61%" />
</colgroup>
<tbody valign="top">
<tr class="row-odd"><td>Data science: a new discipline?</td>
<td><ul class="first last simple">
<li>What disciplines are feeding this new science?</li>
<li>How can this new science help social scientists?</li>
</ul>
</td>
</tr>
<tr class="row-even"><td>Computational methods for data science</td>
<td><ul class="first last simple">
<li>Classification and clustering</li>
<li>Topic modeling</li>
<li>Natural language processing</li>
<li>Linking and matching</li>
<li>Network analysis</li>
</ul>
</td>
</tr>
<tr class="row-odd"><td>The graphical and visual display of data</td>
<td><ul class="first last simple">
<li>Static plots</li>
<li>Programmatic interface and reproducibility</li>
<li>Interactive web-based plots</li>
</ul>
</td>
</tr>
<tr class="row-even"><td>Sources of data for the social sciences</td>
<td><ul class="first last simple">
<li>Survey, secondary, experimental</li>
<li>Data science as a source for new data</li>
<li>Public and private administrative records</li>
<li>On-line experiments</li>
<li>Streaming data</li>
</ul>
</td>
</tr>
<tr class="row-odd"><td>Integrating Data and Social Sciences</td>
<td><ul class="first last simple">
<li>Descriptive versus structural analysis</li>
<li>Predictive verus structural analysis</li>
<li>Counterfactual analysis</li>
</ul>
</td>
</tr>
<tr class="row-even"><td>Reproducibility and transparency</td>
<td><ul class="first last simple">
<li>Programming and code documentation</li>
<li>Presenting results in notebooks and other interactive formats</li>
<li>On-line commenting and continuous updating</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<div class="section" id="prerequisites">
<h2>Prerequisites<a class="headerlink" href="#prerequisites" title="Permalink to this headline">¶</a></h2>
<p>The course is primarily intended for graduate students in the social sciences, but I will consider admitting upper-level undergraduates who demonstrate a strong interest in the course content.</p>
<p>I aim to make the course accessible to students with wide-ranging interests and computational knowledge. It is my hope that a sufficiently diverse group of students enroll so that collectively I can identify and form groups that operate as data-science teams collaborating toward completion of a course project that draws on the diverse talents of all group members.</p>
<p>However, during the first several weeks of the course I will expect students to develop basic familiarity with:</p>
<ul class="simple">
<li>File management and system administration in a Unix (e.g., Linux or macOS) environment</li>
<li>Basic syntax for the Python programming language</li>
<li>Structured Query Language (SQL)</li>
<li>Git for version control</li>
</ul>
<p>There are many fantastic places on this campus (see <a class="reference internal" href="resources.html#resources"><span class="std std-ref">Online Resources and References</span></a>), and on the web (often for free, or small monthly charge) where you can acquire basic working knowledge of these tools. If you know absolutely nothing about any of these topics, you should plan to allocate time for self-study during the early part of the course. However, please don&#8217;t worry if you&#8217;re just getting started; there is always more to learn, and I will encourage lots of classroom interaction to share knowledge.</p>
<div class="toctree-wrapper compound">
</div>
</div>
</div>


           </div>
          </div>
          <footer>
  
    <div class="rst-footer-buttons" role="navigation" aria-label="footer navigation">
      
        <a href="objectives.html" class="btn btn-neutral float-right" title="Learning Objectives" accesskey="n">Next <span class="fa fa-arrow-circle-right"></span></a>
      
      
    </div>
  

  <hr/>

  <div role="contentinfo">
    <p>

    </p>
  </div> 

</footer>

        </div>
      </div>

    </section>

  </div>
  


  

    <script type="text/javascript">
        var DOCUMENTATION_OPTIONS = {
            URL_ROOT:'./',
            VERSION:'',
            COLLAPSE_INDEX:false,
            FILE_SUFFIX:'.html',
            HAS_SOURCE:  true
        };
    </script>
      <script type="text/javascript" src="_static/jquery.js"></script>
      <script type="text/javascript" src="_static/underscore.js"></script>
      <script type="text/javascript" src="_static/doctools.js"></script>
      <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

  

  
  
    <script type="text/javascript" src="_static/js/theme.js"></script>
  

  
  
  <script type="text/javascript">
      jQuery(function () {
          SphinxRtdTheme.StickyNav.enable();
      });
  </script>
   

</body>
</html>