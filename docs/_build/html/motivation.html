

<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->
<head>
  <meta charset="utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <title>Why Take This Class? &mdash; Computational Methods for Economic and Social Data</title>
  

  
  

  

  
  
    

  

  
  
    <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
  

  

  
        <link rel="index" title="Index"
              href="genindex.html"/>
        <link rel="search" title="Search" href="search.html"/>
    <link rel="top" title="Computational Methods for Economic and Social Data" href="index.html"/>
        <link rel="next" title="Course Format" href="course_format.html"/>
        <link rel="prev" title="Course Timeline" href="schedule.html"/> 

  
  <script src="_static/js/modernizr.min.js"></script>

</head>

<body class="wy-body-for-nav" role="document">

  <div class="wy-grid-for-nav">

    
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search">
          

          
            <a href="index.html" class="icon icon-home"> Computational Methods for Economic and Social Data
          

          
          </a>

          
            
            
          

          
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>

          
        </div>

        <div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="main navigation">
          
            
            
                <ul class="current">
<li class="toctree-l1"><a class="reference internal" href="objectives.html">Learning Objectives</a></li>
<li class="toctree-l1"><a class="reference internal" href="topics.html">Course Topics</a></li>
<li class="toctree-l1"><a class="reference internal" href="prerequisites.html">Prerequisites</a></li>
<li class="toctree-l1"><a class="reference internal" href="schedule.html">Course Timeline</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Why Take This Class?</a></li>
<li class="toctree-l1"><a class="reference internal" href="course_format.html">Course Format</a></li>
<li class="toctree-l1"><a class="reference internal" href="resources.html">Online Resources and References</a></li>
<li class="toctree-l1"><a class="reference internal" href="references.html">References</a></li>
</ul>

            
          
        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap">

      
      <nav class="wy-nav-top" role="navigation" aria-label="top navigation">
        <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
        <a href="index.html">Computational Methods for Economic and Social Data</a>
      </nav>


      
      <div class="wy-nav-content">
        <div class="rst-content">
          

 



<div role="navigation" aria-label="breadcrumbs navigation">
  <ul class="wy-breadcrumbs">
    <li><a href="index.html">Docs</a> &raquo;</li>
      
    <li>Why Take This Class?</li>
      <li class="wy-breadcrumbs-aside">
        
          
            <a href="_sources/motivation.rst.txt" rel="nofollow"> View page source</a>
          
        
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
            
  <div class="section" id="why-take-this-class">
<span id="motivation"></span><h1>Why Take This Class?<a class="headerlink" href="#why-take-this-class" title="Permalink to this headline">¶</a></h1>
<p>Social scientists have always worked with data, but the relatively recent arrival of the digital economy presents a new set of data opportunities and challenges <a class="reference internal" href="references.html#einav2014economics" id="id1">[EL14]</a><a class="reference internal" href="references.html#varian2014big" id="id2">[Var14]</a>. The art and science of designing and administering surveys remains important, but the scale and variety of digital data available to researchers today demands a new type researcher: someone who can think analytically about social and economic systems, <em>and</em> who can crawl and scrape the web searching for data, navigate and query a database, apply machine learning algorithms to extract and classify text and other digital artifacts into conceptually meaningful empirical objects, and perhaps do all this on an instance of a cloud computing cluster.</p>
<p>Data Science has emerged as a label to describe an eclectic mix of computational skills and tools needed to meet this demand. So far, the typical individual using this label to describe their field of expertise has formal training in an academic discipline emphasizing computational or inferential aspects of these new data challenges (computer science, engineering, and computational statistics). Researchers in these domains tend to think in terms of &#8220;mining&#8221; data for insights about the future. Data are the oracle, and researchers tend to be agnostic about the underlying social and economic forces generating observed data; they may even be hostile toward using theory about human and system-level behavior as a guide for model specification. The computer scientist might ask,</p>
<blockquote>
<div>“Surely we can&#8217;t know what&#8217;s going at the level of individual behavior, and any model we choose to impose on data will always be wrong. In the face of certain model misspecification, why not maintain maximal flexibility and let the data speak?”</div></blockquote>
<p>While this argument may have merit in some settings, it ignores the possibility that incorporating prior understanding of human behavior into a data model can make feasible an otherwise intractable estimation problem. More importantly, modeling and quantitative measurement linking behavior and policy (public or private) provides the means for normative counterfactual analysis of policy design <a class="reference internal" href="references.html#monroe2015no" id="id3">[MPR+15]</a><a class="reference internal" href="references.html#athey2016state" id="id4">[AI16]</a><a class="reference internal" href="references.html#shiffrin2016drawing" id="id5">[Shi16]</a>. During the onset of the Big Data era, capturing and storing petabyte and terabyte scale data represented a first-order concern. This was a job for engineers and computer scientists. Organizing, classifying, and drawing inferences from these data came later, and in this realm there is lots of room and need for skill sharing, communication, and cross fertilization of ideas among computational, inferential, and social science domains <a class="reference internal" href="references.html#chang2014understanding" id="id6">[CKK14]</a>.</p>
<p>Although mostly focused on tools and application of computational methods, this course differs from most other Data Science offerings by emphasizing an economics and social science perspective where the primary objective of empirical research typically is to make inferences about the veracity of <em>theories</em>, and to provide normative input on matters of economic and social policy. In this context, data are &#8220;digital traces&#8221; of the actions and decisions of human beings that research can use, in conjunction with theory, to estimate and infer structural features for preference relationships, production technologies, friend and job networks, and many other socioeconomic relationships. With this aim, any understanding or new knowledge one hopes to obtain from an estimated data model requires an element of <em>interpretation</em> relating model to theory. This interplay between theorizing about a data generating process (which is necessary for normative interpretation of model estimates), and using outcome data to estimate a particular statistical model, contrasts sharply with machine learning and &#8220;data mining&#8221; approaches that emphasize prediction.</p>
<p>If none of this commentary provides sufficient reason for you to take this course, here are three additional reasons:</p>
<ol class="arabic simple">
<li>Data Science is a relatively new term, but its <a class="reference external" href="https://books.google.com/ngrams/graph?content=Data+Science&amp;year_start=1970&amp;year_end=2008&amp;corpus=15&amp;smoothing=3&amp;share=&amp;direct_url=t1%3B%2CData%20Science%3B%2Cc0">growing use suggests it&#8217;s
here to stay</a>;</li>
<li>If you decide you don&#8217;t want to pursue an academic career, <a class="reference external" href="https://www.fastcompany.com/3055629/the-future-of-work/these-are-the-top-25-jobs-in-the-us-this-year">you&#8217;ll have
plenty of other opportunities</a>;</li>
<li>As social scientists, the data scientists <a class="reference external" href="http://www.nytimes.com/2016/11/10/technology/the-data-said-clinton-would-win-why-you-shouldnt-have-believed-it.html?_r=0">need your help</a>!</li>
</ol>
</div>


           </div>
          </div>
          <footer>
  
    <div class="rst-footer-buttons" role="navigation" aria-label="footer navigation">
      
        <a href="course_format.html" class="btn btn-neutral float-right" title="Course Format" accesskey="n">Next <span class="fa fa-arrow-circle-right"></span></a>
      
      
        <a href="schedule.html" class="btn btn-neutral" title="Course Timeline" accesskey="p"><span class="fa fa-arrow-circle-left"></span> Previous</a>
      
    </div>
  

  <hr/>

  <div role="contentinfo">
    <p>

    </p>
  </div> 

</footer>

        </div>
      </div>

    </section>

  </div>
  


  

    <script type="text/javascript">
        var DOCUMENTATION_OPTIONS = {
            URL_ROOT:'./',
            VERSION:'',
            COLLAPSE_INDEX:false,
            FILE_SUFFIX:'.html',
            HAS_SOURCE:  true
        };
    </script>
      <script type="text/javascript" src="_static/jquery.js"></script>
      <script type="text/javascript" src="_static/underscore.js"></script>
      <script type="text/javascript" src="_static/doctools.js"></script>
      <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

  

  
  
    <script type="text/javascript" src="_static/js/theme.js"></script>
  

  
  
  <script type="text/javascript">
      jQuery(function () {
          SphinxRtdTheme.StickyNav.enable();
      });
  </script>
   

</body>
</html>