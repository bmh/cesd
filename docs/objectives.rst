Learning Objectives
-------------------

Interdisciplinarity forces compromise with respect to depth of coverage. My aim is to make students from business and social science fields familiar enough with computational methods relevant to working with unstructured and "Big" data that they know what is possible, where to look, and what questions to ask when seeking solutions to seemingly intractable data challenges; for students from the computational sciences, I aim to provide increased awareness and understanding of how theory of behavior and markets can be used when building data models and interpreting estimation results.

Specific learning objectives include:

- **Programmatic data retrieval**: Use Python tools to extract data from HTML, XML, and JSON file formats, and to connect with and acquire data from web-based data APIs.

- **Data processing tools**: Provision and use cloud computing resources; basic familiarity with Unix command line tools for file management and data handling; Python programming language for data analysis; essentials of database query.

- **Version control**: Familiarity and elementary use of Git file versioning, repository management, and collaboration tools.

- **Text processing**: Use command line scripting, text-based document preparation tools, and the Python programming language to automate data analysis and manuscript preparation.

- **Computational methods for data analysis**: Demonstrate ability to apply one or more Data Science tools (e.g., classification, clustering, natural language processing) to data, and to interpret results appropriately.

- **Social science theory, model interpretation, and inference**: Use a social science theory or model to guide and facilitate data analysis and interpretation.
