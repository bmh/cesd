.. _motivation:

Why Take This Class?
--------------------

Social scientists have always worked with data, but the relatively recent arrival of the digital economy presents a new set of data opportunities and challenges :cite:`einav2014economics,varian2014big`. The art and science of designing and administering surveys remains important, but the scale and variety of digital data available to researchers today demands a new type researcher: someone who can think analytically about social and economic systems, *and* who can crawl and scrape the web searching for data, navigate and query a database, apply machine learning algorithms to extract and classify text and other digital artifacts into conceptually meaningful empirical objects, and perhaps do all this on an instance of a cloud computing cluster.

Data Science has emerged as a label to describe an eclectic mix of computational skills and tools needed to meet this demand. So far, the typical individual using this label to describe their field of expertise has formal training in an academic discipline emphasizing computational or inferential aspects of these new data challenges (computer science, engineering, and computational statistics). Researchers in these domains tend to think in terms of "mining" data for insights about the future. Data are the oracle, and researchers tend to be agnostic about the underlying social and economic forces generating observed data; they may even be hostile toward using theory about human and system-level behavior as a guide for model specification. The computer scientist might ask,

    “Surely we can't know what's going at the level of individual behavior, and any model we choose to impose on data will always be wrong. In the face of certain model misspecification, why not maintain maximal flexibility and let the data speak?”

While this argument may have merit in some settings, it ignores the possibility that incorporating prior understanding of human behavior into a data model can make feasible an otherwise intractable estimation problem. More importantly, modeling and quantitative measurement linking behavior and policy (public or private) provides the means for normative counterfactual analysis of policy design :cite:`monroe2015no,athey2016state,shiffrin2016drawing`. During the onset of the Big Data era, capturing and storing petabyte and terabyte scale data represented a first-order concern. This was a job for engineers and computer scientists. Organizing, classifying, and drawing inferences from these data came later, and in this realm there is lots of room and need for skill sharing, communication, and cross fertilization of ideas among computational, inferential, and social science domains :cite:`chang2014understanding`.

Although mostly focused on tools and application of computational methods, this course differs from most other Data Science offerings by emphasizing an economics and social science perspective where the primary objective of empirical research typically is to make inferences about the veracity of *theories*, and to provide normative input on matters of economic and social policy. In this context, data are "digital traces" of the actions and decisions of human beings that research can use, in conjunction with theory, to estimate and infer structural features for preference relationships, production technologies, friend and job networks, and many other socioeconomic relationships. With this aim, any understanding or new knowledge one hopes to obtain from an estimated data model requires an element of *interpretation* relating model to theory. This interplay between theorizing about a data generating process (which is necessary for normative interpretation of model estimates), and using outcome data to estimate a particular statistical model, contrasts sharply with machine learning and "data mining" approaches that emphasize prediction.

If none of this commentary provides sufficient reason for you to take this course, here are three additional reasons:

1. Data Science is a relatively new term, but its `growing use suggests it's
   here to stay`_;
2. If you decide you don't want to pursue an academic career, `you'll have
   plenty of other opportunities`_;
3. As social scientists, the data scientists `need your help`_!

.. _growing use suggests it's here to stay:
   https://books.google.com/ngrams/graph?content=Data+Science&year_start=1970&year_end=2008&corpus=15&smoothing=3&share=&direct_url=t1%3B%2CData%20Science%3B%2Cc0

.. _you'll have plenty of other opportunities:
   https://www.fastcompany.com/3055629/the-future-of-work/these-are-the-top-25-jobs-in-the-us-this-year

.. _need your help:
   http://www.nytimes.com/2016/11/10/technology/the-data-said-clinton-would-win-why-you-shouldnt-have-believed-it.html?_r=0


