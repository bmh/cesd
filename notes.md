pedagogy of teaching data analysis: data first!

https://scholar.google.com/scholar?start=20&hl=en&as_sdt=5,50&sciodt=0,50&cites=14855685173904736462&scipsc=

chris alban
https://github.com/bhueth/Data_Bootcamp

Anhai

Not going to teach course, but next fall why not let interested ms students from cs get
Credit for being ds project coders/tas in my course. Could be some easy pubs

Need to get him money first

0. Social Science in the Digital Age: Communication, Coding, and Computation
     - Will your blog get you tenure?
     - Tweeting your way to a Nobel?
     - Documenting impact: DOI, ORCID, Altmetrics, Publon, ImpactStory
     - The text editor, CLI, version control, scripting and scraping
     - Text processing and document preparation
     - Provisioning and managing cloud computing resources


# Student Evaluation and Grading

1. Weekly blog post (with data analysis)
2. Class participation
3. Group project
4. Final individual data science project

# Course Readings

* `[COMPTHINK]`_ Adhikari, Ana and John DeNero with Contributions by David Wagner,
  "Computational and Inferential Thinking"

* `[TURIGUIDE]`_ Fox, Emily and Carlos Guestrin, "Turi Machine Learning Machine Learning Platform User Guide"

* `[BIGSSDATA]`_ Foster, Ian, Rayid Ghani, Ron Jarmin, Frauke Kreuter, and Julia Lane, "Big Data and Social Science: A Practical Guide to Methods and Tool"

* http://kieranhealy.org/files/papers/plain-person-text.pdf
  (or http://plain-text.co/)

* Kieran Healey on Data visualization http://vissoc.co/

https://github.com/tedunderwood/paceofchange

* https://tomaugspurger.github.io/

* Athey and Imbens

* DS Lectures on YouTube

* Science article by Levin

* Articles by Varian

# Additional Learning Resources

- nyu databootcamp
- tom asperger guy in iowa
* http://www.gregreda.com/blog/
* http://pbpython.com/
* https://github.com/jlevy/the-art-of-command-line
* http://datasciencetoolbox.org/
* https://people.duke.edu/~ccc14/sta-663/
* Learning Vim
* Learning Emacs

.. [#] If this commentary doesn't provide sufficient reason for you to take
   this course, here are three additional reasons: i) Data Science is a
   relatively new term, but its `growing use suggests it's here to stay`_; ii)
   if you decide you don't want to pursue an academic career, `you'll have
   plenty of other opportunities`_; iii) as social scientists, the data
   scientists `need your help`_!

.. [#] No, I'm not referring to Windows File Explorer! I include this
   provision, because some students may not have experience with the Unix
   operating system and its variants (e.g., Linux, macOS). I do not require
   that you use Unix, but I *strongly* recommend it and provide links below to
   resources for learning what you need to know. It's quite painless to install
   and run a free version of Linux on a Windows machine, and you'll have
   access to a Linux server for more resource intensive computing needs.

.. _growing use suggests it's here to stay:
   https://books.google.com/ngrams/graph?content=Data+Science&year_start=1970&year_end=2008&corpus=15&smoothing=3&share=&direct_url=t1%3B%2CData%20Science%3B%2Cc0

.. _you'll have plenty of other opportunities:
   https://www.fastcompany.com/3055629/the-future-of-work/these-are-the-top-25-jobs-in-the-us-this-year

.. _need your help:
   http://www.nytimes.com/2016/11/10/technology/the-data-said-clinton-would-win-why-you-shouldnt-have-believed-it.html?_r=0

.. _COMPTHINK: https://www.inferentialthinking.com/

.. _TURIGUIDE: https://turi.com/learn/

.. _BIGSSDATA: http://www.bigdatasocialscience.com/

..

    The set of questions that can be answered with Big Data are severely
    constrained absent theorizing about underlying mechanisms that generate the
    data, as are opportunities to conduct meaningful counterfactual analyses.

    Multiple levels of participation: "everything in the browser"

    Project Ideas:

    - Open Secrets
    - UMETRICS
    - Calag/Tomato
    - InfoGroup/CBP
    - Calag
    - Tomato
    - CBS
    - FAI
    - Meta Analysis of experimental literature in any area (collective action!)
    - AWS 990
    - UW Datamart
    - Involve OSU/UCLA students, work remotely

    Class goals:

    - ORCID id
    - Altmetrics
    - Publish reproducible paper on Winnower end of the semester
    - Weekly/daily updates on Twitter -- hastag wiscdsss
    - Weekly blog post on weekly progress wordpress
    - Collaboration in authorea/bitbucket
    - Groups with eclectic skills/self forming hopefully

    - How data science lost the election!
    - Examples from each of several areas with data
    - Including research papers I'd like to finish!
    - Project: publish paper on Winnower
    - Authorea/all command line tools/Bitbucket/Wordpress/Twitter
    - Science 2.0: Blogging, Social Media, and Peer Review in the Digital Age
    - The Digital Economy Dilemma (and a Solution from Another Era)
    - Reddit and Twitter are classic public goods with tremendous spillover benefits
    - Authorea needs to be a cooperative with service fee model
    - Analogy between Kernel development and open science -- every innovation area has a BDFL.
    - Find a workflow that works for you, describe mine?
    - Reproduce the fantastic text-based life article using vim


    Story Line: Farmers are struggling; small facing big power, need capital... collection action, but can't easily overcome. Policy is the solution.

    Authorea/Winnower: Owned and operated by scientists for scientists through democratic control; suppress investor ownership and it's not a nonprofit!

    The Credibility Conundrum for Open Science (rather than one big reputation, decentralize and exploit lots of micro reputations; we can do this now because the publishing platform is not a constraint).

    Unilateral decision by public universities to send fees to Authorea. Everything is plowed back in;

        - How can (and should) researchers integrate machine learning approaches used by Data Scientists with approaches in the social sciences that eschew "data mining" and instead emphasize the importance of theorizing about underlying forces that produce the data *prior* to hypothesis testing and model estimation? In each domain, researchers use a form of dimensionality reduction to make prediction possible, but are these approaches substitutes, or is there a way to use them together?

- Social scientists face new engineering and system administration challenges when attempting to retrieve, store, document, and reliably version real-time data *streams*. These engineering and system administration challenges compound concerns that already exist in the social sciences regarding the reproducibility of research results. What can be done?

- Many kinds of digital data, though potentially valuable for research, also raise justifiable concerns regarding the potential for abuse and violation of personal privacy.

## Learning Objectives

- Introduce students in the social sciences to tools and techniques needed for "wrangling" messy digital data from disparate sources into a form suitable for statistical and econometric analysis.

- Emphasize the critical need for the social science community to develop strong privacy protection guidelines and technologies, and to communicate effectively the potential benefit of secure and confidential data access for research. Throughout the course, students will learn approaches to computation, data handling, and documentation that support *reproducible science*.






Go through steps and then demonstrate how it can all be scripted that
provisioning and computation occurs automagically

https://haroldsoh.com/2016/04/28/set-up-anaconda-ipython-tensorflow-julia-on-a-google-compute-engine-vm/

- provision compute instance, ubuntu latest LTS
- open cloud shell
- wget https://repo.continuum.io/archive/Anaconda2-4.2.0-Linux-x86_64.sh
- bash Anaconda2
- new cloud shell window
- wget https://repo.continuum.io/archive/Anaconda3-4.2.0-Linux-x86_64.sh
- bash Anaconda3
- create aliases for conda2, conda3, jupyter2, jupyter3, ipython2, ipython3
- conda2 upgrade anaconda
- conda3 upgrade anaconda
- install .continuum licenses
- install .dotfiles - install gcloud sdk and ini

<!-- Social science has an important role to play in efforts to interpret data that are the outcome of social and economic behavior. Emerging technologies for data analysis open up exciting new research opportunities for social scientists. These technologies are a direct response to the recent flood of digital data that stream near continuously from communication and sensing technologies used by individuals, private organizations, and government agencies. New data and technologies offer fantastic research potential, but they also present significant conceptual challenges. Engineers, computer system administrators, and social scientists must speak to one another to make progress on these fronts. Specialization is essential to the creation of new knowledge, but there is now a pressing need for "contact at the boundaries" across relevant disciplinary domains. This course will encourage such contact by equipping participants in the class with enough knowledge to ask the right questions of colleagues in other fields, and to know where to look to learn more. -->



