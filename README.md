# Computational Science for Understanding Economic and Social Data

Welcome.

I hope to convince you all (if you're not already convinced), that there are many good reasons to invest modestly in learning to manage the evolution of your writing and data analysis projects with a version control framework. There are several to choose from, but [Git](https://en.wikipedia.org/wiki/Git) seems to have emerged during the past several years as the most popular, in part because the cloud-based [GitHub](https://en.wikipedia.org/wiki/GitHub) made using Git so painless. Atlassian (maker of rival product, [BitBucket](https://bitbucket.org/)) has a nice set of [Git tutorials](https://www.atlassian.com/git/tutorials). GitLab (where you are now!) is a community-based and free-for-personal-use product that has nearly all the features of the commercial products (and a few very nice features of its own), so we'll use that.

I generally use version control even when working alone, but its benefits are most apparent when I'm collaborating with others. Passing around attachments in emails, or even sharing files on a file synchronization service like Dropbox or Google Drive, make it difficult to follow changes and work asynchronously across collaborators. More importantly, these practices cannot easily support reproduction of analysis and results by others. Working with plain text, and using a platform like GitLab (or GitHub, or Bitbucket) enable code sharing with colleagues and readers who can check, readily see changes, and build on your work.

I'll be posting and updating course materials on the [Wiki](https://gitlab.com/brenthueth/computing-economic-social-data/wikis/home) for this "GitLab Project".


